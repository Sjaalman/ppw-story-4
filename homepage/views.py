from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Muhammad Hanif Fahreza' 
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001, 6, 19) 
npm = 1906351026 

# Create your views here.
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def contact(request):
    return render(request, 'contact.html')

def story1(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'story1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0